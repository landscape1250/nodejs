let p1 = new Promise((resolve, reject) => {
    resolve(42);
});
let p2 = Promise.reject(43);

let p3 = new Promise((resolve, reject) => {
    resolve(44);
});
let p4 = Promise.race([p1, p2, p3]);

p4.catch((value) => {
    console.log(value); // 43
});