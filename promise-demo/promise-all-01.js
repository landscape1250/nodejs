let p1 = new Promise((resolve, reject) => {
    resolve(42);
});
let p2 = new Promise((resolve, reject) => {
    resolve(43);
});
let p3 = new Promise((resolve, reject) => {
    resolve(44);
});
let p4 = Promise.all([p1, p2, p3]);

p4.then((value) => {
    console.log(Array.isArray(value)); // true
    console.log(value[0]); // 42
    console.log(value[1]); // 43
    console.log(value[2]); // 44
});

/* out:

true
42
43
44

*/

p4.then((value) => {
    console.log(Array.isArray(value)); // true
    console.log(value); // [ 42, 43, 44 ]
});

p4.catch((value) => {
    console.log(Array.isArray(value));
    console.log(value[0]);
    console.log(value[1]);
    console.log(value[2]);
});

// nothing to return
