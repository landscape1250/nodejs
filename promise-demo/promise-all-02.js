let p1 = new Promise((resolve, reject) => {
    resolve(42);
});
let p2 = new Promise((resolve, reject) => {
    reject(43);
});
let p3 = new Promise((resolve, reject) => {
    resolve(44);
});
let p4 = Promise.all([p1, p2, p3]);

p4.then((value) => {
    console.log(Array.isArray(value))
    console.log(value[0]);
});
/* out: 
(node:69341) UnhandledPromiseRejectionWarning: 43
(node:69341) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing insideof an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 2)
(node:69341) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.

*/

p4.catch((value) => {
    console.log(Array.isArray(value)) // false
    console.log(value); // 43
});
/* out:
false
43
*/